package com.devcamp.cdailycampaign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdailycampaignApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdailycampaignApplication.class, args);
	}

}
