package com.devcamp.cdailycampaign.controllers;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin(value="*", maxAge = -1)
@RequestMapping("/")
@RestController
public class CDailyCampaign {
   
    @GetMapping("/promotion")
    public String getInfor(@RequestParam(value = "name", defaultValue = "Các Bợm Nghiện Pizza") String fullName) {
        DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today = LocalDate.now(ZoneId.systemDefault());

        String formattedDay = today.format(dtfVietnam);
        if (formattedDay.equals("Thứ Hai")) {
            return String.format("Hello %s ! Hôm nay %s, mua 1 tặng 1.", fullName, formattedDay);
        } else if (formattedDay.equals("Thứ Ba")) {
            return String.format("Hello %s ! Hôm nay %s, tặng một phần bánh ngọt.", fullName, formattedDay);
        } else if (formattedDay.equals("Thứ Tư")) {
            return String.format("Hello %s ! Hôm nay %s, tặng một phần nước ngọt.", fullName, formattedDay);
        } else if (formattedDay.equals("Thứ Năm")) {
            return String.format("Hello %s ! Hôm nay %s, tặng một phần salad.", fullName, formattedDay);
        } else if (formattedDay.equals("Thứ Sáu")) {
            return String.format("Hello %s ! Hôm nay %s, tặng một phần nước ngọt.", fullName, formattedDay);
        } else if (formattedDay.equals("Thứ Bảy")) {
            return String.format("Hello %s ! Hôm nay %s, tặng một phần salad.", fullName, formattedDay);
        } else if (formattedDay.equals("Chủ Nhật")) {
            return String.format("Hello %s ! Hôm nay %s, tặng một phần sườn.", fullName, formattedDay);
        } else {
            return String.format("Hello %s ! Hôm nay %s, hết khuyến mãi.", fullName, formattedDay);
        }

    }

}
